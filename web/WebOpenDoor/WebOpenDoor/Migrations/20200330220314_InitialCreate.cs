﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebOpenDoor.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Door",
                columns: table => new
                {
                    DoorId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nome = table.Column<string>(maxLength: 150, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Door", x => x.DoorId);
                });

            migrationBuilder.CreateTable(
                name: "DoorAccessLog",
                columns: table => new
                {
                    DoorAccessLogId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DoorId = table.Column<int>(nullable: false),
                    UserRfId = table.Column<string>(maxLength: 30, nullable: false),
                    Reason = table.Column<string>(maxLength: 254, nullable: false),
                    DateLog = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DoorAccessLog", x => x.DoorAccessLogId);
                });

            migrationBuilder.CreateTable(
                name: "DoorPermission",
                columns: table => new
                {
                    DoorPermissionId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DoorId = table.Column<int>(nullable: false),
                    UserRfId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DoorPermission", x => x.DoorPermissionId);
                });

            migrationBuilder.CreateTable(
                name: "Usuario",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserRfId = table.Column<string>(maxLength: 30, nullable: false),
                    UserName = table.Column<string>(maxLength: 254, nullable: true),
                    Login = table.Column<string>(maxLength: 50, nullable: true),
                    Password = table.Column<string>(maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Usuario", x => x.UserId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Door");

            migrationBuilder.DropTable(
                name: "DoorAccessLog");

            migrationBuilder.DropTable(
                name: "DoorPermission");

            migrationBuilder.DropTable(
                name: "Usuario");
        }
    }
}
