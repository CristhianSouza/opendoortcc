﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebOpenDoor.Context;
using WebOpenDoor.Controllers.Base;
using WebOpenDoor.Models;

namespace WebOpenDoor.Controllers
{
    [Authorize]
    public class DoorController : BaseController
    {
        private readonly WebDbContext _webDbContext;
        public DoorController(WebDbContext webDbContext)
        {
            _webDbContext = webDbContext;
        }

        public IActionResult Index()
        {
            return View();
        }

        public JsonResult ObterDoors()
        {
            var doors = _webDbContext.Door.Where(w => w.Active);
            return Json(new { data = doors });
        }

        public PartialViewResult ObterDoorForm(int doorId)
        {
            Door porta = null;
            if (doorId > 0)
                porta = _webDbContext.Door.Find(doorId);

            return PartialView("~/Views/Door/Partials/_ModalDoorForm.cshtml", porta);
        }

        public JsonResult SalvarDoor(Door porta)
        {
            try
            {
                if (porta.DoorId > 0)
                    _webDbContext.Door.Update(porta);
                else
                    _webDbContext.Door.Add(porta);

                _webDbContext.SaveChanges();

                return ObterJsonMessage(true);
            }
            catch (Exception ex)
            {
                return ObterJsonMessage(false, ex.Message);
            }
        }

        public string ObterNomeDoor(int doorId)
        {
            var door = _webDbContext.Door.Find(doorId);
            return door?.Nome;
        }

        public JsonResult DeletarDoor(int doorId)
        {
            try
            {
                var porta = _webDbContext.Door.Find(doorId);
                porta.Active = false;
                _webDbContext.Door.Update(porta);
                _webDbContext.SaveChanges();

                return ObterJsonMessage(true);
            }
            catch (Exception ex)
            {
                return ObterJsonMessage(false, ex.Message);
            }
        }
    }
}