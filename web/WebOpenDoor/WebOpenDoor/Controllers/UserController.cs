﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebOpenDoor.Context;
using WebOpenDoor.Controllers.Base;
using WebOpenDoor.Models;

namespace WebOpenDoor.Controllers
{
    [Authorize]
    public class UserController : BaseController
    {
        private readonly WebDbContext _webDbContext;
        public UserController(WebDbContext webDbContext)
        {
            _webDbContext = webDbContext;
        }

        public IActionResult Index()
        {
            return View();
        }

        public JsonResult ObterUsers()
        {
            var users = _webDbContext.User.Where(w => w.Active).ToList();
            var doors = _webDbContext.Door.Where(w => w.Active).ToList();

            foreach (var user in users)
            {
                var idPermitidas = _webDbContext.DoorPermission.Where(w => w.UserRfId == user.UserRfId).Select(s => s.DoorId);
                user.Doors = doors.Where(w => idPermitidas.Contains(w.DoorId));
            }
            return Json(new { data = users });
        }

        public PartialViewResult ObterUserForm(int userId)
        {
            User usuario;
            if (userId > 0)
            {
                usuario = _webDbContext.User.Find(userId);
                var idPermitidas = _webDbContext.DoorPermission.Where(w => w.UserRfId == usuario.UserRfId).Select(s => s.DoorId);
                usuario.Doors = _webDbContext.Door.Where(w => idPermitidas.Contains(w.DoorId) && w.Active);
            }
            else
            {
                usuario = new User
                {
                    Doors = new List<Door>()
                };
            }
            ViewData["listaPortas"] = _webDbContext.Door.ToList();

            return PartialView("~/Views/User/Partials/_ModalUserForm.cshtml", usuario);
        }

        public JsonResult SalvarUser(User usuario)
        {
            try
            {
                List<DoorPermission> listaPermissoes = new List<DoorPermission>();
                usuario.Doors ??= new List<Door>();
                foreach(var door in usuario.Doors)
                    listaPermissoes.Add(new DoorPermission { DoorId = door.DoorId, UserRfId = usuario.UserRfId });

                if (usuario.UserId > 0)
                {
                    _webDbContext.User.Update(usuario);
                    _webDbContext.DoorPermission.RemoveRange(_webDbContext.DoorPermission.Where(w => w.UserRfId == usuario.UserRfId));
                    _webDbContext.DoorPermission.AddRange(listaPermissoes);

                    if (string.IsNullOrEmpty(usuario.Login))
                    {
                        var entityEntry = _webDbContext.Entry(usuario);
                        entityEntry.Property("Login").IsModified = false;
                        entityEntry.Property("Password").IsModified = false;
                    }
                }
                else
                {
                    _webDbContext.User.Add(usuario);
                    _webDbContext.DoorPermission.AddRange(listaPermissoes);
                }

                _webDbContext.SaveChanges();

                return ObterJsonMessage(true);
            }
            catch (Exception ex)
            {
                return ObterJsonMessage(false, ex.Message);
            }
        }

        public string ObterNomeUser(int userId)
        {
            var user = _webDbContext.User.Find(userId);
            return user?.UserName;
        }

        public JsonResult DeletarUser(int userId)
        {
            try
            {
                var usuario = _webDbContext.User.Find(userId);
                _webDbContext.DoorPermission.RemoveRange(_webDbContext.DoorPermission.Where(w => w.UserRfId == usuario.UserRfId));
                usuario.Active = false;
                _webDbContext.User.Update(usuario);
                _webDbContext.SaveChanges();

                return ObterJsonMessage(true);
            }
            catch (Exception ex)
            {
                return ObterJsonMessage(false, ex.Message);
            }
        }
    }
}