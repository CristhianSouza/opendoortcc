﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using WebOpenDoor.Context;
using WebOpenDoor.ViewModels;

namespace WebOpenDoor.Controllers
{
    public class AuthorizeController : Controller
    {
        private readonly WebDbContext _context;
        public AuthorizeController(WebDbContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }
        
        [HttpPost]
        public IActionResult Login(UserLoginViewModel user)
        {
            if (!ModelState.IsValid) 
                return View(user);

            var resultLogin = _context.User.FirstOrDefault(w => w.Login.ToUpper() == user.Login.ToUpper() && w.Password == user.Password.ToUpper());

            if(resultLogin != null)
            {
                var identity = new ClaimsIdentity("ApplicationCookie");
                identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, resultLogin.Login));
                identity.AddClaim(new Claim(ClaimTypes.Name, resultLogin.UserName));
                identity.AddClaim(new Claim(ClaimTypes.Sid, resultLogin.UserId.ToString()));

                var principalClaims = new ClaimsPrincipal(identity);
                var authProperties = new AuthenticationProperties
                {
                    AllowRefresh = true,
                    IsPersistent = true,
                    ExpiresUtc = DateTime.UtcNow.AddHours(15)
                };

                HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principalClaims, authProperties);
                return RedirectToAction("Index", "Control");
            }

            return RedirectToAction("Login");
        }

        public IActionResult Logout()
        {
            HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction("Login");
        }
    }
}