﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebOpenDoor.Context;
using WebOpenDoor.Controllers.Base;
using WebOpenDoor.Models;

namespace WebOpenDoor.Controllers
{
    [Authorize]
    public class AccessLogController : BaseController
    {
        private readonly WebDbContext _webDbContext;
        public AccessLogController(WebDbContext webDbContext)
        {
            _webDbContext = webDbContext;
        }

        public IActionResult Index()
        {
            return View();
        }

        public JsonResult ObterLogs()
        {
            var accessLogs = _webDbContext.DoorAccessLog.ToList();
            accessLogs.ForEach(f =>
            {
                f.Door = _webDbContext.Door.Find(f.DoorId);
                f.User = _webDbContext.User.FirstOrDefault(w => w.UserRfId ==  f.UserRfId);

            });

            return Json(new { data = accessLogs });
        }
    }
}