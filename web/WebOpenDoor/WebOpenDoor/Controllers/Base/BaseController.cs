﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WebOpenDoor.Controllers.Base
{
    [Authorize]
    public class BaseController : Controller
    {
        public JsonResult ObterJsonMessage(bool saved, string message)
        {
            return Json(new { saved, message });
        }

        public JsonResult ObterJsonMessage(bool saved)
        {
            var message = saved ? "Salvo com sucesso." : "Não foi possível completar a solicitação. Tente novamente mais tarde.";
            return Json(new { saved = true, message });
        }
    }
}