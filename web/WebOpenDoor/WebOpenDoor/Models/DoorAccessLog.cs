﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebOpenDoor.Models
{
    public class DoorAccessLog
    {
        [Key]
        public int DoorAccessLogId { get; set; }

        [Required]
        public int DoorId { get; set; }

        [Required]
        [MaxLength(30)]
        public string UserRfId { get; set; }

        [Required]
        [MaxLength(254)]
        public string Reason { get; set; }

        [Required]
        public DateTime DateLog { get; set; }

        [NotMapped]
        public Door Door { get; set; }

        [NotMapped]
        public User User { get; set; }

        [NotMapped]
        public string DateLogFormatted
        {
            get 
            {
                return DateLog.ToString("dd/MM/yyyy HH:mm");
            }
        }
    }
}
