﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebOpenDoor.Models
{
    public class User
    {
        [Key]
        public int UserId { get; set; }

        [Required]
        [MaxLength(30)]
        public string UserRfId { get; set; }

        [MaxLength(254)]
        public string UserName { get; set; }

        [MaxLength(50)]
        public string Login { get; set; }

        public bool Active { get; set; }

        [MaxLength(100)]
        public string Password { get; set; }

        [NotMapped]
        public IEnumerable<Door> Doors { get; set; }
    }
}
