﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebOpenDoor.Models
{
    public class Door
    {
        [Key]
        public int DoorId { get; set; }

        [Required]
        [MaxLength(150)]
        public string Nome { get; set; }

        public bool Active { get; set; }
    }
}
