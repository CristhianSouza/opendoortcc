﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebOpenDoor.Models
{
    public class DoorPermission
    {
        [Key]
        public int DoorPermissionId { get; set; }

        [Required]
        public int DoorId { get; set; }

        [Required]
        public string UserRfId { get; set; }
    }
}
