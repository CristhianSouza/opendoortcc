﻿$(document).ready(function () {
    $("#gridDoors").DataTable({
        ajax: {
            url: "/Door/ObterDoors",
            type: "GET"
        },
        columns: [
            {
                data: "nome",
                title: "Descrição",
                width: "90%"
            },
            {
                data: "doorId",
                title: "Gerenciar",
                render: function (data, type, row, meta) {
                    return "<a href='#' title='Editar' class='btn btn-info' style='padding: 2px 5px' onclick='abrirModalDoorForm(" + data + ")'><i class='fas fa-pencil-alt'></i></a>" +
                           "<a href='#' title='Excluir' class='btn btn-danger' style='margin-left: 2px; padding: 2px 5px' onclick='abrirModalDeleteDoor(" + data + ")'><i class='fas fa-trash'></i></a>";
                },
                className: "text-center",
                orderable: false,
                width: "10%"
            }
        ],
        bLengthChange: false,
        searching: false,
        pageLength: 7
    });
});

function abrirModalDoorForm(id) {
    var btns = '<button type="button" class="btn btn-info" onclick="salvarDoor()">Salvar</button>' +
        '<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>';

    var title = id > 0 ? 'Alterar porta' : 'Incluir porta';

    $.ajax({
        url: "/Door/ObterDoorForm",
        data: {
            doorId: id
        },
        type: "GET",
        success: function (content) {
            $("#modalTitle").html(title);
            $("#modalBody").html(content);
            $("#modalFooter").html(btns);

            $("#modal").modal("show");
        },
        error: function () {
            showAlertError();
        }
    });
}

function salvarDoor() {
    var nome = $("#Nome").val();
    if (nome == null || nome == "") {
        showMessageAjax(false, "É necessário preencher um nome para a Porta.", "msgAjaxModal");
    }
    else {
        var porta = new Object();
        porta.DoorId = $("#DoorId").val();
        porta.Active = $("#Active").val();
        porta.Nome = nome;

        $.ajax({
            url: "/Door/SalvarDoor",
            data: porta,
            type: "POST",
            success: function (data) {
                showMessageAjax(data.saved, data.message);
                reloadGrid("gridDoors");

                $("#modal").modal("hide");
            },
            error: function () {
                showAlertError();
            }
        });
    }
}

function abrirModalDeleteDoor(id) {
    $.ajax({
        url: "/Door/ObterNomeDoor",
        data: {
            doorId: id
        },
        type: "GET",
        success: function (data) {
            $("#modalTitle").html("Remover porta");
            $("#modalBody").html("Tem certeza que deseja remover a porta <b>" + data + "</b>?");
            $("#modalFooter").html('<button type="button" class="btn btn-info" onclick="deletarDoor(' + id + ')">Remover</button>' +
                '<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>');

            reloadGrid("gridDoors");
            $("#modal").modal("show");
        },
        error: function () {
            showAlertError();
        }
    });
}

function deletarDoor(id) {
    $.ajax({
        url: "/Door/DeletarDoor",
        data: {
            doorId: id
        },
        type: "POST",
        success: function (data) {
            showMessageAjax(data.saved, data.message);
            reloadGrid("gridDoors");

            $("#modal").modal("hide");
        },
        error: function () {
            showAlertError();
        }
    });
}