﻿$(document).ready(function () {
    $("#gridAccessLog").DataTable({
        ajax: {
            url: "/AccessLog/ObterLogs",
            type: "GET"
        },
        columns: [
            {
                data: "dateLogFormatted",
                title: "Acessado em",
                width: "20%"
            },
            {
                data: "door.nome",
                title: "Porta",
                width: "20%"
            },
            {
                data: "user.userName",
                title: "Usuário",
                width: "25%"
            },
            {
                data: "reason",
                title: "Motivo",
                width: "35%"
            }
        ],
        bLengthChange: false,
        searching: false,
        pageLength: 7
    });
});

function abrirModalDoorForm(id) {
    var btns = '<button type="button" class="btn btn-info" onclick="salvarDoor()">Salvar</button>' +
        '<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>';

    var title = id > 0 ? 'Alterar porta' : 'Incluir porta';

    $.ajax({
        url: "/Door/ObterDoorForm",
        data: {
            doorId: id
        },
        success: function (content) {
            $("#modalTitle").html(title);
            $("#modalBody").html(content);
            $("#modalFooter").html(btns);

            $("#modal").modal("show");
        },
        error: function () {
            showAlertError();
        }
    });
}

function salvarDoor() {
    var nome = $("#Nome").val();
    if (nome == null || nome == "") {
        showMessageAjax(false, "É necessário preencher um nome para a Porta.", "msgAjaxModal");
    }
    else {
        var porta = new Object();
        porta.DoorId = $("#DoorId").val();
        porta.Nome = nome;

        $.ajax({
            url: "/Door/SalvarDoor",
            data: porta,
            success: function (data) {
                showMessageAjax(data.saved, data.message);
                reloadGrid("gridDoors");

                $("#modal").modal("hide");
            },
            error: function () {
                showAlertError();
            }
        });
    }
}

function abrirModalDeleteDoor(id) {
    $.ajax({
        url: "/Door/ObterNomeDoor",
        data: {
            doorId: id
        },
        success: function (data) {
            $("#modalTitle").html("Remover porta");
            $("#modalBody").html("Tem certeza que deseja remover a porta <b>" + data + "</b>?");
            $("#modalFooter").html('<button type="button" class="btn btn-info" onclick="deletarDoor(' + id + ')">Remover</button>' +
                '<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>');

            reloadGrid("gridDoors");
            $("#modal").modal("show");
        },
        error: function () {
            showAlertError();
        }
    });
}

function deletarDoor(id) {
    $.ajax({
        url: "/Door/DeletarDoor",
        data: {
            doorId: id
        },
        success: function (data) {
            showMessageAjax(data.saved, data.message);
            reloadGrid("gridDoors");

            $("#modal").modal("hide");
        },
        error: function () {
            showAlertError();
        }
    });
}