﻿$(document).ready(function () {
    $("#gridUsers").DataTable({
        ajax: {
            url: "/User/ObterUsers",
            type: "GET"
        },
        autoWidth: false,
        columns: [
            {
                data: "userRfId",
                title: "Crachá",
                width: "15%"
            },
            {
                data: "userName",
                title: "Nome",
                width: "30%"
            },
            {
                data: "doors",
                title: "Portas permitidas",
                render: function (data, type, row, meta) {
                    var permitidas = data.map(function (obj) { return obj.nome; }).join(", ");
                    return permitidas;
                },
                width: "25%"
            },
            {
                data: "login",
                title: "Login",
                width: "15%"
            },
            {
                data: "userId",
                title: "Gerenciar",
                render: function (data, type, row, meta) {
                    return "<a href='#' title='Editar' class='btn btn-info' style='padding: 2px 5px' onclick='abrirModalUserForm(" + data + ")'><i class='fas fa-pencil-alt'></i></a>" +
                        "<a href='#' title='Excluir' class='btn btn-danger' style='margin-left: 2px; padding: 2px 5px' onclick='abrirModalDeleteUser(" + data + ")'><i class='fas fa-trash'></i></a>";
                },
                className: "text-center",
                orderable: false,
                width: "10%"
            }
        ],
        bLengthChange: false,
        searching: false,
        pageLength: 7
    });
});

function abrirModalUserForm(id) {
    var btns = '<button type="button" class="btn btn-info" onclick="salvarUser()">Salvar</button>' +
        '<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>';

    var title = id > 0 ? 'Alterar usuário' : 'Incluir usuário';

    $.ajax({
        url: "/User/ObterUserForm",
        data: {
            userId: id
        },
        type: "GET",
        success: function (content) {
            $("#modalTitle").html(title);
            $("#modalBody").html(content);
            $("#modalFooter").html(btns);

            $("#modal").modal("show");
        },
        error: function () {
            showAlertError();
        }
    });
}

function salvarUser() {
    var nome = $("#UserName").val();
    var rfid = $("#UserRfId").val();
    if (nome == null || nome == "") {
        showMessageAjax(false, "É necessário preencher o nome do usuário.", "msgAjaxModal");
    }
    else if (rfid == null || rfid == "") {
        showMessageAjax(false, "É necessário preencher o crachá do usuário.", "msgAjaxModal");
    }
    else {
        var usuario = new Object();
        var doorsCheckeds = $("input[name=DoorIdPermission]:checked");
        var doors = [];
        for (var i = 0; i < doorsCheckeds.length; i++) {
            doors.push({ DoorId: $(doorsCheckeds[i]).val(), Nome: null });
        }

        usuario.UserId = $("#UserId").val();
        usuario.Active = $("#Active").val();
        usuario.UserName = nome;
        usuario.UserRfId = rfid;
        usuario.Login = $("#Login").val();
        usuario.Password = $("#Password").val();
        usuario.Doors = doors;

        $.ajax({
            url: "/User/SalvarUser",
            data: usuario,
            type: "POST",
            success: function (data) {
                showMessageAjax(data.saved, data.message);
                reloadGrid("gridUsers");

                $("#modal").modal("hide");
            },
            error: function () {
                showAlertError();
            }
        });
    }
}

function abrirModalDeleteUser(id) {
    $.ajax({
        url: "/User/ObterNomeUser",
        data: {
            userId: id
        },
        type: "GET",
        success: function (data) {
            $("#modalTitle").html("Remover usuário");
            $("#modalBody").html("Tem certeza que deseja remover o usuário <b>" + data + "</b>?");
            $("#modalFooter").html('<button type="button" class="btn btn-info" onclick="deletarUser(' + id + ')">Remover</button>' +
                '<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>');

            reloadGrid("gridUsers");
            $("#modal").modal("show");
        },
        error: function () {
            showAlertError();
        }
    });
}

function deletarUser(id) {
    $.ajax({
        url: "/User/DeletarUser",
        data: {
            userId: id
        },
        type: "POST",
        success: function (data) {
            showMessageAjax(data.saved, data.message);
            reloadGrid("gridUsers");

            $("#modal").modal("hide");
        },
        error: function () {
            showAlertError();
        }
    });
}