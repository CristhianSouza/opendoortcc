﻿function showMessageAjax(success, msg, divId = "msgAjax") {
    var status = success ? "OK:" : "ERRO:";
    var classe = success ? "success" : "danger";

    $("#" + divId).html('<div class="alert alert-' + classe + '">' +
        '<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>' +
        '<strong>' + status + '</strong> ' + msg +
        '</div>');
}

function reloadGrid(gridId) {
    $("#" + gridId).DataTable().ajax.reload();
}

function showAlertError() {
    alert("ERRO: Falha na comunicação com o servidor. Tente novamente mais tarde.");
}