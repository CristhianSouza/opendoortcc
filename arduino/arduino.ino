#include <SPI.h>
#include <MFRC522.h>

#define SS_PIN 10
#define RST_PIN 9
MFRC522 mfrc522(SS_PIN, RST_PIN);  // Create MFRC522 instance.

void setup() {
  Serial.begin(9600);  // Initialize serial communications with the PC
  SPI.begin();         // Init SPI bus
  mfrc522.PCD_Init();  // Init MFRC522 card
  pinMode(7, OUTPUT);  // Iniciando Lampada Vermelha Ligada no Pino 7
  pinMode(6, OUTPUT);  // Iniciando Lampada Verde Ligada no Pino 6
}

void loop() {

  String receiveVal = "";     
  if(Serial.available())
  {
    receiveVal = Serial.readString();     
    if(receiveVal == "5b59c6624a33d6a3585c1d1b5f3aa399") {
       // ACENDER LUZ VERDE
       // Liberar Acesso
       digitalWrite(6, HIGH);  
       delay(2000);  
       digitalWrite(6, LOW);  
       delay(2000);  
    }
    else if(receiveVal == "cb46874a4259053af083cae2d1b18483"){
       // ACENDER LUZ VERMELHA
       // Negar Acesso
       digitalWrite(7, HIGH);  
       delay(2000);  
       digitalWrite(7, LOW);  
       delay(2000);  
    }
    Serial.print(receiveVal);
  } 
   
  // Look for new cards
  if (!mfrc522.PICC_IsNewCardPresent()) {
    return;
  }

  // Select one of the cards
  if (!mfrc522.PICC_ReadCardSerial()) {
    return;
  }

  String conteudo = "";
  for(byte i = 0; i < mfrc522.uid.size; i++){
    conteudo.concat(String(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " "));
    conteudo.concat(String(mfrc522.uid.uidByte[i], HEX));
  }
  conteudo.toUpperCase();
  Serial.print(conteudo);
  delay(2000);   
}