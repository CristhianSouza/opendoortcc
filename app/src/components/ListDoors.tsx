import React from 'react'
import { IonCard } from '@ionic/react'

import { DoorType } from '../generated/graphql'
import styles from './Doors.module.scss'

interface Props {
  doors: DoorType
}

const ListDoors: React.FC<Props> = props => {
  const { doors } = props

  return (
    <IonCard
      button
      className={styles.cardAzul}
      routerLink={`/openDoors/${doors.doorId}`}
    >
      <p> {doors.nome} </p>
    </IonCard>
  )
}

export default ListDoors
