import React from 'react'
import { useDoorsGetAllQuery } from '../generated/graphql'
import ListDoors from './ListDoors'
import { IonLoading } from '@ionic/react'

const ListDoorsContainer: React.FC = () => {
  const { data, loading } = useDoorsGetAllQuery()

  if (loading) {
    return <IonLoading isOpen={loading} message="Carregando..." />
  }

  return (
    <>
      {data &&
        data.doors.map(door => <ListDoors key={door.doorId} doors={door} />)}
    </>
  )
}

export default ListDoorsContainer
