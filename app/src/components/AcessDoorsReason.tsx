import React, { useState } from 'react'
import {
  IonCard,
  IonCardHeader,
  IonCardSubtitle,
  IonCardTitle,
  IonCardContent,
  IonItem,
  IonLabel,
  IonTextarea,
  IonButton,
  IonText,
} from '@ionic/react'

import { DoorAcessType } from '../generated/graphql'
import styles from './Doors.module.scss'

interface Props {
  doorAcess: DoorAcessType
}

const AcessDoorsReason: React.FC<Props> = props => {
  const { doorAcess } = props
  const [reason, setReason] = useState<string>('')

  return (
    doorAcess.acess ? (
      <IonCard button className={styles.card}>
        <IonCardHeader>
          <IonCardSubtitle>Solicitação de Acesso</IonCardSubtitle>
          <IonCardTitle>{doorAcess.nome}</IonCardTitle>
        </IonCardHeader>
        <IonCardContent>
          <IonItem lines="none">
            <IonLabel>Motivo</IonLabel>
          </IonItem>
          <IonItem lines="none">
            <IonTextarea
              rows={3}
              placeholder="Insira o motivo do acesso..."
              onIonChange={(e: any) => setReason(e.target.value)}
              ></IonTextarea>
          </IonItem>
          <IonButton
            color="tertiary"
            className={styles.btn}
            expand="block"
            routerLink={`/openDoors/${doorAcess.doorId}/${doorAcess.userRfId}/${reason}`}
            >
            Acessar
          </IonButton>
        </IonCardContent>
      </IonCard> 
    )
    : 
    (
      <IonCard button className={styles.card}>
        <IonCardHeader>
          <IonCardSubtitle>Solicitação de Acesso</IonCardSubtitle>
          <IonCardTitle>{doorAcess.nome}</IonCardTitle>
        </IonCardHeader>
        <IonCardContent>
          <IonText>
            {doorAcess.resultMsg}
          </IonText>
        </IonCardContent>
      </IonCard> 
    )
  )
}

export default AcessDoorsReason
