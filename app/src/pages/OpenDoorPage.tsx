import React from 'react'
import {
  IonPage,
  IonHeader,
  IonToolbar,
  IonTitle,
  IonContent,
  IonButtons,
  IonBackButton,
  IonLoading,
  IonButton,
  IonIcon,
} from '@ionic/react'
import { useParams } from 'react-router'

import {
  useDoorAcessPermissionQuery,
  DoorAcessType,
} from '../generated/graphql'
import AcessDoorsReason from '../components/AcessDoorsReason'

import { home } from "ionicons/icons";

const OpenDoorPage: React.FC = () => {
  const { id } = useParams<{ id: string }>()

  const { data, loading } = useDoorAcessPermissionQuery({
    variables: { id },
  })

  if (loading) {
    return (
      <IonLoading isOpen={loading} message="Insira seu cartão de acesso..." />
    )
  }

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonButton href="/openDoors"><IonIcon slot="start" icon={home} /></IonButton>
          </IonButtons>
          <IonTitle> Acesso </IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent className="ion-padding">
        <AcessDoorsReason
          doorAcess={data!.doorAcessPermissiion as DoorAcessType}
        />
      </IonContent>
    </IonPage>
  )
}

export default OpenDoorPage
