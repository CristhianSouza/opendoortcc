import React from 'react'
import {
  IonPage,
  IonHeader,
  IonToolbar,
  IonTitle,
  IonContent,
  IonButtons,
  IonBackButton,
  IonLoading,
  IonButton,
  IonIcon,
} from '@ionic/react'
import { useParams } from 'react-router'

import { useDoorGrantAcessQuery } from '../generated/graphql'
import AcessDoorsGrant from '../components/AcessDoorsGrant'

import { home } from "ionicons/icons";

const AcessDoorsGrantPage: React.FC = () => {
  const { id } = useParams<{ id: string }>()
  const { userRfId } = useParams<{ userRfId: string }>()
  const { reason } = useParams<{ reason: string }>()

  const { loading } = useDoorGrantAcessQuery({
    variables: {
      id,
      userRfId,
      reason,
    },
  })

  if (loading) {
    return <IonLoading isOpen={loading} message="Liberando acesso..." />
  }

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonButton href="/openDoors"><IonIcon slot="start" icon={home} /></IonButton>
          </IonButtons>
          <IonTitle> Solicitação de Abertura </IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent className="ion-padding">
        <AcessDoorsGrant />
      </IonContent>
    </IonPage>
  )
}

export default AcessDoorsGrantPage
