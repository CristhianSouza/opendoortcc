import {
  IonContent,
  IonHeader,
  IonPage,
  IonTitle,
  IonToolbar,
} from '@ionic/react'
import React from 'react'
import './DoorsList.css'
import ListDoorsContainer from '../components/ListDoorsContainer'

const DoorsListPage: React.FC = () => {
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Open Door Mobile</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <ListDoorsContainer />
      </IonContent>
    </IonPage>
  )
}

export default DoorsListPage
