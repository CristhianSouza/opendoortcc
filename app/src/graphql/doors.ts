import gql from 'graphql-tag'

export const DOORS_GETALL_QUERY = gql`
  query DoorsGetAll {
    doors {
      doorId
      nome
    }
  }
`

export const DOOR_GET_QUERY = gql`
  query DoorGet($id: ID!) {
    door(id: $id) {
      doorId
      nome
    }
  }
`

export const DOOR_ACESS_PERMISSION_QUERY = gql`
  query DoorAcessPermission($id: ID!) {
    doorAcessPermissiion(id: $id) {
      acess
      doorId
      nome
      resultMsg
      userRfId
    }
  }
`

export const DOOR_GRANT_ACESS_QUERY = gql`
  query DoorGrantAcess($id: ID!, $userRfId: String, $reason: String) {
    doorGrantAcess(id: $id, userRfId: $userRfId, reason: $reason) {
      acess
      doorId
      nome
      resultMsg
      userRfId
      motivo
    }
  }
`
