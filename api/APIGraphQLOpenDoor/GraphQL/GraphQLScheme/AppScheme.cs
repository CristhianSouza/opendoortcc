﻿using APIGraphQLOpenDoor.GraphQL.GraphQLQueries;
using GraphQL;
using GraphQL.Types;

namespace APIGraphQLOpenDoor.GraphQL.GraphQLScheme
{
    public class AppScheme : Schema
    {
        public AppScheme(IDependencyResolver resolver) : base(resolver)
        {
            Query = resolver.Resolve<AppQuery>();
        }
    }
}