﻿using APIGraphQLOpenDoor.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace APIGraphQLOpenDoor.Interfaces
{
    public interface IDoorRepository
    {
        IEnumerable<Door> GetAll();
        Door Get(int doorId);
        Task<DoorAcess> GetAcessPermission(int doorId);
        Task<DoorAcess> GrantAcess(int doorId, string userRfId, string motivo);
    }
}
