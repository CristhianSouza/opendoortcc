﻿using System;
using System.IO.Ports;
using System.Threading.Tasks;

namespace APIGraphQLOpenDoor.Arduino
{
    public static class ArduinoUtil
    {
        static bool _exit = false;
        static bool run = false;
        const string CodeOpenDoor = "5b59c6624a33d6a3585c1d1b5f3aa399";
        const string CodeCloseDoor = "cb46874a4259053af083cae2d1b18483";
        static string rfid = "";

        public static string ReadRfId()
        {
            _exit = false;
            run = false;
            string[] ports = SerialPort.GetPortNames();
            SerialPort port = new SerialPort(ports[0], 9600, Parity.None, 8, StopBits.One);
            port.Open();
            while (!_exit)
            {
                if (!run)
                {
                    port.DataReceived += new SerialDataReceivedEventHandler(RFID_DataReceivedHandler);
                    run = true;
                }
            }
            port.Close();
            return rfid;
        }

        public static async Task<int> OpenDoor()
        {
            return await CallArduino(CodeOpenDoor);
        }

        public static async Task<int> CloseDoor()
        {
           return await CallArduino(CodeCloseDoor);
        }

        public static async Task<int> CallArduino(string code)
        {
            string[] ports = SerialPort.GetPortNames();
            SerialPort port = new SerialPort(ports[0], 9600, Parity.None, 8, StopBits.One);
            port.Open();
            port.Write(code);
            //Tempo necessário para os bytes seren enviado ao arduino
            System.Threading.Thread.Sleep(TimeSpan.FromSeconds(1));
            port.Close();
            return await Task.FromResult(0);
        }

        static void RFID_DataReceivedHandler(object sender, SerialDataReceivedEventArgs e)
        {
            SerialPort sp = (SerialPort)sender;
            string indata = sp.ReadExisting();
            indata = indata.Trim();
            rfid = indata;
            _exit = true;
        }
    }
}
