﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace APIGraphQLOpenDoor.Entities
{
    public class DoorAcess
    {
        public int DoorId { get; set; }

        [Required]
        [MaxLength(150)]
        public string Nome { get; set; }

        public bool Acess { get; set; }

        public string UserRfId { get; set; }

        public string ResultMsg { get; set; }
        public string Motivo { get; set; }
    }
}