﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIGraphQLOpenDoor.Entities
{
    public class DoorPermission
    {
        public int DoorPermissionId { get; set; }
        public int DoorId { get; set; }
        public string UserRfId { get; set; }
    }
}
